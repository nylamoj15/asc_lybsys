import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ReportsOverdueComponent } from './reports-overdue.component';

describe('ReportsOverdueComponent', () => {
  let component: ReportsOverdueComponent;
  let fixture: ComponentFixture<ReportsOverdueComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ReportsOverdueComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReportsOverdueComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
