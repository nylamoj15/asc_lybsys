import { Component, OnInit, Input} from '@angular/core';
import { FileRenderComponent } from '../../file-render/file-render.component';
import { NgForm } from '@angular/forms';
import { DatePipe } from '@angular/common';
import { ServerrequestService } from '../../services/serverrequest.service';
import { RouterModule, Routes, ActivatedRoute, ParamMap, Router } from '@angular/router';
import { NgbModal, ModalDismissReasons, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { FormBuilder, Validators } from '@angular/forms';
import { NgxSelectModule, INgxSelectOptions } from 'ngx-select-ex';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';

@Component({
  selector: 'app-borrowed-books',
  templateUrl: './borrowed-books.component.html',
  styleUrls: ['./borrowed-books.component.css']
})
export class BorrowedBooksComponent implements OnInit {

	modalReference: NgbModalRef;
	datePipeEn: DatePipe = new DatePipe('en-US');

	DateNow = this.datePipeEn.transform(Date.now(), 'yyyy-MM-dd');

	BorrowedBooks;
	borrowedbook_id;

  constructor(
  	public SrvrRqst : ServerrequestService,
  	private router: Router,
  	private modalService: NgbModal) { }

  ngOnInit() {
  	this.GetAllActiveBorrowedBooks();
  	console.log(this.DateNow);
  }

  GoToPage(page){
    this.router.navigate([page])
    console.log(page);
  }

  OpenModal(content, id){
    this.modalReference = this.modalService.open(content, {size: 'sm', centered: true });
    this.borrowedbook_id = id;
    console.log(this.borrowedbook_id);	
  }

  GetAllActiveBorrowedBooks(){
  	this.SrvrRqst.GetAllActiveBorrowedBooks().subscribe((res => {
  		console.log(res);
  		this.BorrowedBooks = res;
  	}))
  }

  ReturnBook(){
  	this.SrvrRqst.ReturnBook(this.borrowedbook_id).subscribe((res => {
  		console.log(res);
  		this.modalReference.close();
  		this.GetAllActiveBorrowedBooks();
  	}))
  }

}
