import { Component, OnInit, Input} from '@angular/core';
import { FileRenderComponent } from '../../file-render/file-render.component';
import { NgForm } from '@angular/forms';
import { DatePipe } from '@angular/common';
import { ServerrequestService } from '../../services/serverrequest.service';
import { RouterModule, Routes, ActivatedRoute, ParamMap, Router } from '@angular/router';
import { NgbModal, ModalDismissReasons, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { FormBuilder, Validators } from '@angular/forms';
import { NgxSelectModule, INgxSelectOptions } from 'ngx-select-ex';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {

	ActiveMembers;
	BorrowedBooks;
	ReservedBooks;
	LostBooks;
	ActiveBorrows;
	ActiveReserved;

  constructor(
  	public SrvrRqst : ServerrequestService,
  	private router: Router,
  	private modalService: NgbModal
  	) { }

  ngOnInit() {
  	this.CountActiveMembers();
  	this.CountBorrowedBooks();
  	this.CountReservedBooks();
  	this.CountLostBooks();
  	this.CountActiveBorrows();
  	this.CountActiveReserved();
  }

  async CountActiveMembers(){
  	this.SrvrRqst.CountActiveMembers().subscribe((res => {
  		this.ActiveMembers = res.total;
  	}))
  }

  async CountBorrowedBooks(){
  	this.SrvrRqst.CountBorrowedBooks().subscribe((res => {
  		this.BorrowedBooks = res.total;
  	}))
  }

  async CountReservedBooks(){
  	this.SrvrRqst.CountReservedBooks().subscribe((res => {
  		this.ReservedBooks = res.total;
  	}))
  }

  async CountLostBooks(){
  	this.SrvrRqst.CountLostBooks().subscribe((res => {
  		this.LostBooks = res.total;
  	}))
  }

  async CountActiveBorrows(){
  	this.SrvrRqst.CountActiveBorrows().subscribe((res => {
  		this.ActiveBorrows = res.total;
  	}))
  }

  async CountActiveReserved(){
  	this.SrvrRqst.CountActiveReserved().subscribe((res => {
  		this.ActiveReserved = res.total;
  	}))
  }





}
