import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ReportsBooksnotreturnedComponent } from './reports-booksnotreturned.component';

describe('ReportsBooksnotreturnedComponent', () => {
  let component: ReportsBooksnotreturnedComponent;
  let fixture: ComponentFixture<ReportsBooksnotreturnedComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ReportsBooksnotreturnedComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReportsBooksnotreturnedComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
