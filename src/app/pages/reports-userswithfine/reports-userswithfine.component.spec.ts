import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ReportsUserswithfineComponent } from './reports-userswithfine.component';

describe('ReportsUserswithfineComponent', () => {
  let component: ReportsUserswithfineComponent;
  let fixture: ComponentFixture<ReportsUserswithfineComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ReportsUserswithfineComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReportsUserswithfineComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
