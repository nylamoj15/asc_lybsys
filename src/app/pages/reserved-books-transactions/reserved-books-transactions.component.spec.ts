import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ReservedBooksTransactionsComponent } from './reserved-books-transactions.component';

describe('ReservedBooksTransactionsComponent', () => {
  let component: ReservedBooksTransactionsComponent;
  let fixture: ComponentFixture<ReservedBooksTransactionsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ReservedBooksTransactionsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReservedBooksTransactionsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
