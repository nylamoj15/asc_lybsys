import { Component, OnInit, Input} from '@angular/core';
import { FileRenderComponent } from '../../file-render/file-render.component';
import { NgForm } from '@angular/forms';
import { DatePipe } from '@angular/common';
import { ServerrequestService } from '../../services/serverrequest.service';
import { RouterModule, Routes, ActivatedRoute, ParamMap, Router } from '@angular/router';
import { NgbModal, ModalDismissReasons, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { FormBuilder, Validators } from '@angular/forms';
import { NgxSelectModule, INgxSelectOptions } from 'ngx-select-ex';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';


@Component({
  selector: 'app-reserved-books-transactions',
  templateUrl: './reserved-books-transactions.component.html',
  styleUrls: ['./reserved-books-transactions.component.css']
})
export class ReservedBooksTransactionsComponent implements OnInit {

	ReservedTransactions;

  constructor(
  	public SrvrRqst : ServerrequestService,
  	private router: Router,
  	private modalService: NgbModal
  	) { }

  ngOnInit() {
  	this.GetAllReservationTransactions();
  }

  async GetAllReservationTransactions(){
  	this.SrvrRqst.GetAllReservationTransactions().subscribe((res => {
  		this.ReservedTransactions = res;
  		console.log(res);
  	}))
  }

}
