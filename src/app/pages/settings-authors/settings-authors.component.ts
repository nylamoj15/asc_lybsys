import { Component, OnInit, Input, ViewChild, TemplateRef} from '@angular/core';
import { FileRenderComponent } from '../../file-render/file-render.component';
import { NgForm } from '@angular/forms';
import { ServerrequestService } from '../../services/serverrequest.service';
import { RouterModule, Routes, ActivatedRoute, ParamMap, Router } from '@angular/router';
import { NgbModal, ModalDismissReasons, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { FormBuilder, Validators } from '@angular/forms';
import { NgxSelectModule, INgxSelectOptions } from 'ngx-select-ex';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';

@Component({
  selector: 'app-settings-authors',
  templateUrl: './settings-authors.component.html',
  styleUrls: ['./settings-authors.component.css']
})
export class SettingsAuthorsComponent implements OnInit {

	modalReference: NgbModalRef;

	Authors;
	AuthorInfo;
	UpdatedAuthorInfo;
	NewAuthor;

  constructor(
  	public SrvrRqst : ServerrequestService,
  	private router: Router,
  	private modalService: NgbModal
  	) { }

  ngOnInit() {
  	this.GetAllAuthors();
  }

  OpenModal(content, Author){
  	this.modalReference = this.modalService.open(content, {size: 'lg', centered: true });
  	this.AuthorInfo = Author;
  }

  async GetAllAuthors(){
  	this.SrvrRqst.GetAllAuthors().subscribe((res => {
  		this.Authors = res.data;
  		console.log(this.Authors)
  	}))
  }

  async UpdateAuthor(Author){
  	console.log(Author.value);
  	this.UpdatedAuthorInfo = Author.value;
  	this.SrvrRqst.UpdateAuthor(this.UpdatedAuthorInfo, this.AuthorInfo.author_id).subscribe((res => {
  		console.log(res);
  		this.modalReference.close();
  	}))
  }

  async AddAuthor(NewAuthor){
  	this.NewAuthor = NewAuthor.value;
  }

}
