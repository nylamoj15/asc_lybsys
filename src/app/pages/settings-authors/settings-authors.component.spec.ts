import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SettingsAuthorsComponent } from './settings-authors.component';

describe('SettingsAuthorsComponent', () => {
  let component: SettingsAuthorsComponent;
  let fixture: ComponentFixture<SettingsAuthorsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SettingsAuthorsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SettingsAuthorsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
