import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ReportsCheckedoutComponent } from './reports-checkedout.component';

describe('ReportsCheckedoutComponent', () => {
  let component: ReportsCheckedoutComponent;
  let fixture: ComponentFixture<ReportsCheckedoutComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ReportsCheckedoutComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReportsCheckedoutComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
