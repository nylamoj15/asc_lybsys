import { Component, OnInit, Input} from '@angular/core';
import { FileRenderComponent } from '../../file-render/file-render.component';
import { NgForm } from '@angular/forms';
import { DatePipe } from '@angular/common';
import { ServerrequestService } from '../../services/serverrequest.service';
import { RouterModule, Routes, ActivatedRoute, ParamMap, Router } from '@angular/router';
import { NgbModal, ModalDismissReasons, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { FormBuilder, Validators } from '@angular/forms';
import { NgxSelectModule, INgxSelectOptions } from 'ngx-select-ex';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import { ExcelService } from '../../services/excel.service';



@Component({
  selector: 'app-reports-lost',
  templateUrl: './reports-lost.component.html',
  styleUrls: ['./reports-lost.component.css']
})
export class ReportsLostComponent implements OnInit {

	datePipeEn: DatePipe = new DatePipe('en-US');

	DateNow = this.datePipeEn.transform(Date.now(), 'yyyy-MM-dd');

	LostBooks;

  constructor(
  	public SrvrRqst : ServerrequestService,
  	private router: Router,
  	private modalService: NgbModal,
  	private excelService: ExcelService) { }

  ngOnInit() {
  	this.GetLostBooks();
  }

  GoToPage(page){
    this.router.navigate([page])
    console.log(page);
  }

  async GetLostBooks(){
  	this.SrvrRqst.GetLostBooks().subscribe((res => {
  		this.LostBooks = res[0];
  	}))
  }

  exportAsXLSX():void {
    this.excelService.exportAsExcelFile(this.LostBooks, 'LostBooks-'+this.DateNow);
  }

}
