import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ReportsLostComponent } from './reports-lost.component';

describe('ReportsLostComponent', () => {
  let component: ReportsLostComponent;
  let fixture: ComponentFixture<ReportsLostComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ReportsLostComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReportsLostComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
