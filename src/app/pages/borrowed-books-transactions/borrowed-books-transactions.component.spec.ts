import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BorrowedBooksTransactionsComponent } from './borrowed-books-transactions.component';

describe('BorrowedBooksTransactionsComponent', () => {
  let component: BorrowedBooksTransactionsComponent;
  let fixture: ComponentFixture<BorrowedBooksTransactionsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BorrowedBooksTransactionsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BorrowedBooksTransactionsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
