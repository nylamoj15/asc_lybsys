import { Component, OnInit, Input} from '@angular/core';
import { FileRenderComponent } from '../../file-render/file-render.component';
import { NgForm } from '@angular/forms';
import { DatePipe } from '@angular/common';
import { ServerrequestService } from '../../services/serverrequest.service';
import { RouterModule, Routes, ActivatedRoute, ParamMap, Router } from '@angular/router';
import { NgbModal, ModalDismissReasons, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { FormBuilder, Validators } from '@angular/forms';
import { NgxSelectModule, INgxSelectOptions } from 'ngx-select-ex';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';

@Component({
  selector: 'app-reserved-books',
  templateUrl: './reserved-books.component.html',
  styleUrls: ['./reserved-books.component.css']
})
export class ReservedBooksComponent implements OnInit {

	modalReference: NgbModalRef;
	datePipeEn: DatePipe = new DatePipe('en-US');

	DateNow = this.datePipeEn.transform(Date.now(), 'yyyy-MM-dd');
	ReservedBooks;
	reservation_id;
	CheckoutBookInfo;


  constructor(
  	public SrvrRqst : ServerrequestService,
  	private router: Router,
  	private modalService: NgbModal
  	) {

  }

  ngOnInit() {
  	this.GetAllActiveReservedBooks();
  }

  GoToPage(page){
    this.router.navigate([page])
    console.log(page);
  }

  GetAllActiveReservedBooks(){
  	this.SrvrRqst.GetAllActiveReservedBooks().subscribe((res => {
      // console.log(res);
  		this.ReservedBooks = res;
  		console.log(this.ReservedBooks);
  	}))
  }

  OpenModal(content, id){
    this.modalReference = this.modalService.open(content, {size: 'sm', centered: true });
    this.reservation_id = id;
    console.log(this.reservation_id);	
  }

  CheckoutBook(date_to_return){
  	this.CheckoutBookInfo = {
  		date_borrowed: this.DateNow,
  		date_to_return: date_to_return
  	}
  	console.log(this.CheckoutBookInfo);
  	console.log(this.reservation_id);
  	this.SrvrRqst.CheckoutBook(this.CheckoutBookInfo, this.reservation_id).subscribe((res => {
  		console.log(res);
  		this.modalReference.close();
  		this.GetAllActiveReservedBooks();
  	}))
  }

  CancelReservation(){
  	console.log(this.reservation_id);
  	this.SrvrRqst.CancelReservation(this.reservation_id).subscribe((res => {
  		console.log(res);
  	}))
  }

}
