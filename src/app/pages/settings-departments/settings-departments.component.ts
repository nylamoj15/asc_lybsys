import { Component, OnInit, Input, ViewChild, TemplateRef} from '@angular/core';
import { FileRenderComponent } from '../../file-render/file-render.component';
import { NgForm } from '@angular/forms';
import { ServerrequestService } from '../../services/serverrequest.service';
import { RouterModule, Routes, ActivatedRoute, ParamMap, Router } from '@angular/router';
import { NgbModal, ModalDismissReasons, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { FormBuilder, Validators } from '@angular/forms';
import { NgxSelectModule, INgxSelectOptions } from 'ngx-select-ex';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';

@Component({
  selector: 'app-settings-departments',
  templateUrl: './settings-departments.component.html',
  styleUrls: ['./settings-departments.component.css']
})
export class SettingsDepartmentsComponent implements OnInit {

	modalReference: NgbModalRef;

	Departments;
	DepartmentInfo;
	DepartmentUpdatedInfo;
	NewDepartment;

	status: number = 0;

  constructor(
  	public SrvrRqst : ServerrequestService,
  	private router: Router,
  	private modalService: NgbModal
  	) { }

  ngOnInit() {
  	this.GetAllDepartments();
  }

  OpenModal(content, Department){
  	this.modalReference = this.modalService.open(content, {size: 'lg', centered: true });
  	this.DepartmentInfo = Department;	
  }

  async GetAllDepartments(){
  	this.SrvrRqst.GetAllDepartments().subscribe((res => {
  		// console.log(res.data);
  		this.Departments = res.data;
  	}))
  }

  async UpdateDepartment(DepartmentEditForm: NgForm){
  	this.DepartmentUpdatedInfo = DepartmentEditForm.value;
  	this.DepartmentUpdatedInfo.dept_id = this.DepartmentInfo.dept_id;
  	this.SrvrRqst.UpdateDepartment(this.DepartmentUpdatedInfo).subscribe((res => {
  		// console.log("Updated Successfully");
  		this.GetAllDepartments();
  		this.modalReference.close();
  	}))
  }

  async AddDepartment(DepartmentAddForm:NgForm){
  	this.NewDepartment = DepartmentAddForm.value;
  	this.SrvrRqst.AddNewDepartment(this.NewDepartment).subscribe((res => {
      // console.log(res);
      this.GetAllDepartments();
      this.modalReference.close();
  	}))
  }

}
