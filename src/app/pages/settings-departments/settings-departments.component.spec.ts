import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SettingsDepartmentsComponent } from './settings-departments.component';

describe('SettingsDepartmentsComponent', () => {
  let component: SettingsDepartmentsComponent;
  let fixture: ComponentFixture<SettingsDepartmentsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SettingsDepartmentsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SettingsDepartmentsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
