import { Component, OnInit, Input} from '@angular/core';
import { FileRenderComponent } from '../../file-render/file-render.component';
import { NgForm } from '@angular/forms';
import { DatePipe } from '@angular/common';
import { ServerrequestService } from '../../services/serverrequest.service';
import { RouterModule, Routes, ActivatedRoute, ParamMap, Router } from '@angular/router';
import { NgbModal, ModalDismissReasons, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { FormBuilder, Validators } from '@angular/forms';
import { NgxSelectModule, INgxSelectOptions } from 'ngx-select-ex';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';

@Component({
  selector: 'app-books',
  templateUrl: './books.component.html',
  styleUrls: ['./books.component.css']
})
export class BooksComponent implements OnInit {


  datePipeEn: DatePipe = new DatePipe('en-US');


  modalReference: NgbModalRef;
  BookInfo;
  UpdatedBookInfo;
  Authors;
  Members;
  User;
  member_id;
  FullName: string = "";
  DateToday = this.datePipeEn.transform(Date.now(), 'yyyy-MM-dd HH:mm:ss');
  DateNow = this.datePipeEn.transform(Date.now(), 'yyyy-MM-dd');
  b_date_to_return;

  book_borrowed_info;
  book_reserved_info;

	Books;

  constructor(
  	public SrvrRqst : ServerrequestService,
  	private router: Router,
  	private modalService: NgbModal
  	) {
  }

  ngOnInit() {
  	this.GetAllBooks();
    this.GetAllAuthors();
    this.GetAllMembers();

    console.log(this.DateToday);

  }

  GoToPage(page){
    this.router.navigate([page])
    console.log(page);
  }

  async GetAllBooks(){
  	this.SrvrRqst.GetAllBooks().subscribe((res => {
  		this.Books = res.data;
  		console.log(this.Books);
  	}))
  }

  async GetAllAuthors(){
    this.SrvrRqst.GetAllAuthors().subscribe((res => {
      this.Authors = res.data;
    }))
  }

  async GetAllMembers(){
    this.SrvrRqst.GetAllMembers().subscribe((res => {
      this.Members = res.data;
      console.log(res.data);
    }))
  }

  OpenModal(content, Book){
    this.modalReference = this.modalService.open(content, {size: 'lg', centered: true });
    this.BookInfo = Book;
  }

  OpenModalBook(content, book_id){
    this.modalReference = this.modalService.open(content, {size: 'lg', centered: true });

  }

  UpdateBook(bookeditform: NgForm){
    this.UpdatedBookInfo = bookeditform.value;
    console.log(this.UpdatedBookInfo);
  }

  config = {
    displayKey:"id_number", //if objects array passed which key to be displayed defaults to description
    search:true, //true/false for the search functionlity defaults to false,
    height: 'auto', //height of the list so that if there are more no of items it can show a scroll defaults to auto. With auto height scroll will never appear
    placeholder:'Select', // text to be displayed when no item is selected defaults to Select,
    customComparator: ()=>{}, // a custom function using which user wants to sort the items. default is undefined and Array.sort() will be used in that case, // a number thats limits the no of options displayed in the UI similar to angular's limitTo pipe
    moreText: 'more' ,// text to be displayed whenmore than one items are selected like Option 1 + 5 more
    noResultsFound: 'No results found!', // text to be displayed when no items are found while searching
    searchPlaceholder:'Search', // label thats displayed in search input,
    searchOnKey: 'id_number' // key on which search should be performed this will be selective search. if undefined this will be extensive search on all keys
  }

  selectionChanged(event){
    this.User = event.value[0];
    this.FullName = this.User.first_name + " " + this.User.last_name;
    console.log(this.User);
  }

  BorrowBook(borrowbookform: NgForm, date_to_return){
    this.book_borrowed_info = borrowbookform.value;
    this.book_borrowed_info.date_to_return = date_to_return;
    this.book_borrowed_info.member_id = this.User.member_id;
    this.book_borrowed_info.date_borrowed = this.DateNow;

    // console.log(this.book_borrowed_info);

    this.SrvrRqst.BorrowBook(this.book_borrowed_info).subscribe((res => {
      // console.log(res);
      this.modalReference.close();
      this.GetAllBooks();
    }))  
  }

  ReserveBook(reservebookform: NgForm, date_requested){
    let book_id = reservebookform.value.r_book_id;

    this.book_reserved_info = { member_id: this.User.member_id, date_requested: date_requested, date_of_reservation: this.DateNow, book_id: book_id, status: 'Reserved' }

    // console.log(this.book_reserved_info);

    this.SrvrRqst.ReserveBook(this.book_reserved_info).subscribe((res=>{
      console.log(res);
      this.modalReference.close();
      this.GetAllBooks();
    }))

  }




}
