import { Component, OnInit, Input} from '@angular/core';
import { FileRenderComponent } from '../../file-render/file-render.component';
import { NgbModal, ModalDismissReasons, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { FormBuilder, Validators } from '@angular/forms';
import { NgxSelectModule, INgxSelectOptions } from 'ngx-select-ex';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';

@Component({
  selector: 'app-worklist',
  templateUrl: './worklist.component.html',
  // encapsulation: ViewEncapsulation.None,
  styleUrls: ['./worklist.component.css']
})
export class WorklistComponent implements OnInit {

  closeResult: string;
  modalReference: NgbModalRef;

  //Config for the Smart DropDown
  dropDownconfig = {
    displayKey:"description",
    search:true, 
    height: 'auto', 
    placeholder:'Select', 
    customComparator: ()=>{}, 
    moreText: 'more', 
    noResultsFound: 'No results found!', 
    searchPlaceholder:'Search' 
  }

  //List of all Sites Available
  siteOptions = [
    'The Silk Residences',
    'The Olive Place',
    'The Ivy Wall Hotel'
  ];

  //List of all Rooms depends on the Site clicked
  roomOptions = [
    'Room 13AB',
    'Room 13BC',
    'Room 13DE',
    'Room 13FG'
  ];

  //List of all Categories
  categoryOptions = [
    'Door',
    'Window',
    'Stairs'
  ]

  //Settings for the Worklist Equipment Details
  WorklistSettings = {
    actions: {
      columnTitle: 'Actions'
    },
    add: {
      addButtonContent: "<img src='assets/icons/add.png'>",
      createButtonContent: "<img src='assets/icons/save.png'>",
      cancelButtonContent: "<img src='assets/icons/cancel.png'>",

    },
    edit: {
      editButtonContent: "<img src='assets/icons/pencil.png'>",
      saveButtonContent: "<img src='assets/icons/save.png'>",
      cancelButtonContent: "<img src='assets/icons/cancel.png'>",
    },
    delete: {
      deleteButtonContent: "<img src='assets/icons/trash-bin.png'>",
      confirmDelete: true
    },
    columns: {
      item: {
        title: 'Item'
      },
      reminder: {
        title: 'Reminder'
      },
      condition: {
        title: 'Condition',
        type: 'html',
        editor: {
          type: 'list',
          config: {
            list: [
              {value: "Normal", title: 'Normal'},
              {value: "Not Normal", title: 'Not Normal'}
            ],
          },
        }
      },
      findings: {
        title: 'Findings'
      },
      photo: {
        title: 'Photo',
        type: 'html',
        filter: false,
        editor: {
          type: 'custom',
          component: FileRenderComponent,
        },
      }
    },
    attr: {
      class: 'table table-striped'
    }
  };

  //Default WorkList Data
  WorkListData = [];
  worklistForm;

  constructor(
    private modalService: NgbModal,
    private fb: FormBuilder
  ) {

    this.worklistForm = fb.group({
      siteModel: ['', Validators.required],
      roomModel: ['', Validators.required],
      categoryModel: ['', Validators.required]
    });

  }

  ngOnInit() {
  }

  openVerticallyCentered(content) {
    this.modalService.open(content, { centered: true });
  }

  //Submit Worklist Button
  SubmitWorkList(form, content){
    // console.log(form);
    // console.log(content);
    this.modalReference = this.modalService.open(content, { centered: true });
  }

  

}
