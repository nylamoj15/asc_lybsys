import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ReportsTopusersComponent } from './reports-topusers.component';

describe('ReportsTopusersComponent', () => {
  let component: ReportsTopusersComponent;
  let fixture: ComponentFixture<ReportsTopusersComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ReportsTopusersComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReportsTopusersComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
