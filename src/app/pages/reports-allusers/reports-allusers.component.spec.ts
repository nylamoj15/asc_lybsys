import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ReportsAllusersComponent } from './reports-allusers.component';

describe('ReportsAllusersComponent', () => {
  let component: ReportsAllusersComponent;
  let fixture: ComponentFixture<ReportsAllusersComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ReportsAllusersComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReportsAllusersComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
