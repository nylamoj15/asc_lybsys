import { Component, OnInit, Input} from '@angular/core';
import { FileRenderComponent } from '../../file-render/file-render.component';
import { NgForm } from '@angular/forms';
import { ServerrequestService } from '../../services/serverrequest.service';
import { RouterModule, Routes, ActivatedRoute, ParamMap, Router } from '@angular/router';
import { NgbModal, ModalDismissReasons, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { FormBuilder, Validators } from '@angular/forms';
import { NgxSelectModule, INgxSelectOptions } from 'ngx-select-ex';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';


@Component({
  selector: 'app-members',
  templateUrl: './members.component.html',
  styleUrls: ['./members.component.css']
})

export class MembersComponent implements OnInit {

	
  status: number = 0;
  gender: string = "Female";
  usertype_id: number = 3;
  dept_id: number = 1;

	modalReference: NgbModalRef;

  Members;
  UserTypes;
	MemberInfo;
	Departments;
	NewMember;
  Member_id;
  AccountInfo;
  UpdatedAccountSettings;
 

  constructor(
  	public SrvrRqst : ServerrequestService,
  	private router: Router,
  	private modalService: NgbModal
  	) { }

  ngOnInit() {
  	this.GetAllMembers();
  	this.GetAllDepartments();
    this.GetAllUserType();
  }

  OpenModal(content, Member){
  	this.modalReference = this.modalService.open(content, {size: 'lg', centered: true });
  	this.MemberInfo = Member;
  }

  OpenAccountModal(content, id){
    this.SrvrRqst.GetUserByID(id).subscribe((res => {
      // console.log(res);
      this.AccountInfo = res;
      this.modalReference = this.modalService.open(content, {size: 'lg', centered: true });
    }))
  }

  async GetAllMembers(){
  	this.SrvrRqst.GetAllMembers().subscribe((res => {
  		this.Members = res.data;
  		//console.log(res.data);
  	}))
  }

  async GetAllDepartments(){
  	this.SrvrRqst.GetAllDepartments().subscribe((res => {
  		this.Departments = res.data;
  		//console.log(this.Departments);
  	}))
  }

  async UpdateMember(memberform: NgForm){
  	console.log(memberform.value);
  	this.SrvrRqst.UpdateMember(memberform.value, this.MemberInfo.member_id)
  	.subscribe((res => {
  		if (res[0] === "success") {
  			// console.log("UPDATE SUCCESS");
  			this.modalReference.close();
        this.GetAllMembers();
  		}
  		else {
  			// console.log("UPDATE FAILED")
  		}
  		// console.log(res);
  	}))
  }

  async AddMember(addmemberform: NgForm){
  	this.NewMember = addmemberform.value;
  	this.NewMember.password = addmemberform.value.id_number;
  	this.SrvrRqst.AddMember(this.NewMember)
  	.subscribe((res => {
  		// console.log(res);
      this.GetAllMembers();
      this.modalReference.close();
  	}))
  }

  async UpdateAccountSettings(membersettingsform: NgForm){
    this.UpdatedAccountSettings = membersettingsform.value;
    this.UpdatedAccountSettings.id_number = this.AccountInfo.id_number;
    this.UpdatedAccountSettings.user_id = this.AccountInfo.user_id;
    this.SrvrRqst.UpdateUser(this.UpdatedAccountSettings).subscribe((res => {
      // console.log(res);
      this.modalReference.close();
    }))
  }

  async GetAllUserType(){
    this.SrvrRqst.GetAllUserType().subscribe((res => {
      this.UserTypes = res.data;
    }))
  }


}

