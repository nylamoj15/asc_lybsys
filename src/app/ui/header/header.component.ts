import { Component, OnInit } from '@angular/core';
import { RouterModule, Routes, ActivatedRoute, ParamMap, Router } from '@angular/router';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {

  constructor(
  	private router: Router) { }

  ngOnInit() {
  }

  Logout(){
  	localStorage.removeItem("FN");
  	localStorage.removeItem("MID");
  	localStorage.removeItem("UT");
  	this.router.navigate(['login']);
  }

}


