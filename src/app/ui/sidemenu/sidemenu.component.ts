import { Component, OnInit, Input} from '@angular/core';
import { FileRenderComponent } from '../../file-render/file-render.component';
import { NgForm } from '@angular/forms';
import { ServerrequestService } from '../../services/serverrequest.service';
import { RouterModule, Routes, ActivatedRoute, ParamMap, Router } from '@angular/router';
import { NgbModal, ModalDismissReasons, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { FormBuilder, Validators } from '@angular/forms';
import { NgxSelectModule, INgxSelectOptions } from 'ngx-select-ex';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
@Component({
  selector: 'app-sidemenu',
  templateUrl: './sidemenu.component.html',
  styleUrls: ['./sidemenu.component.css']
})
export class SidemenuComponent implements OnInit {

  FN: string;
  
  GoToPage(page){
    this.router.navigate([page])
    console.log(page);
  }

  
  Pages = [
    { id: 1, page: 'Dashboard', link: "/home/dashboard", status: true },
    { id: 2, page: 'Members', link: "/home/members", status: true },
    { id: 3, page: 'Books', link: "/home/books", status: true },
    { id: 4, page: 'Reports', link: "/home/reports", status: true },
    { id: 5, page: 'OPAC', link: "/home/opac", status: true },
    { id: 6, page: 'Settings', link: "/home/settings", status: true }
  ]

  constructor(
    public SrvrRqst : ServerrequestService,
    private router: Router
    ) {
    this.FN = localStorage.getItem("FN");
    console.log(this.FN);
  }

  ngOnInit() {
  }

}
