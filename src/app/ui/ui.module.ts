import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HashLocationStrategy, LocationStrategy, CommonModule  } from '@angular/common';
import { LayoutComponent } from './layout/layout.component';
import { HeaderComponent } from './header/header.component';
import { FooterComponent } from './footer/footer.component';
import { SidemenuComponent } from './sidemenu/sidemenu.component';

import { HomeComponent } from '../home/home.component';
import { WorklistComponent } from '../pages/worklist/worklist.component';
import { StatusComponent } from '../pages/status/status.component';
import { MembersComponent } from '../pages/members/members.component';
import { BooksComponent } from '../pages/books/books.component';
import { LoginComponent } from '../login/login.component';
import { SettingsComponent } from '../pages/settings/settings.component';
import { DashboardComponent } from '../pages/dashboard/dashboard.component';
import { ReportsComponent } from '../pages/reports/reports.component';
import { OpacComponent } from '../pages/opac/opac.component';
import { ReservedBooksComponent } from '../pages/reserved-books/reserved-books.component';
import { ReservedBooksTransactionsComponent } from '../pages/reserved-books-transactions/reserved-books-transactions.component';
import { BorrowedBooksComponent } from '../pages/borrowed-books/borrowed-books.component';
import { BorrowedBooksTransactionsComponent } from '../pages/borrowed-books-transactions/borrowed-books-transactions.component'; 
import { ProfileComponent } from '../pages/profile/profile.component';

import { ReportsAllusersComponent } from '../pages/reports-allusers/reports-allusers.component';
import { ReportsBooksnotreturnedComponent } from '../pages/reports-booksnotreturned/reports-booksnotreturned.component';
import { ReportsCheckedoutComponent } from '../pages/reports-checkedout/reports-checkedout.component';
import { ReportsLostComponent } from '../pages/reports-lost/reports-lost.component';
import { ReportsOverdueComponent } from '../pages/reports-overdue/reports-overdue.component';
import { ReportsTopusersComponent } from '../pages/reports-topusers/reports-topusers.component';
import { ReportsUserswithfineComponent } from '../pages/reports-userswithfine/reports-userswithfine.component';
 
 
const appRoutes: Routes = [
  {
    path: 'home',
    component: HomeComponent,
    children: [
      {
        path: 'worklist',
        component: WorklistComponent
      },
      {
        path: 'status',
        component: StatusComponent
      },
      {
        path: 'members',
        component: MembersComponent
      },
      {
        path: 'books',
        component: BooksComponent
      },
      {
        path: 'settings',
        component: SettingsComponent
      },
      {
        path: 'dashboard',
        component: DashboardComponent
      },
      {
        path: 'reports',
        component: ReportsComponent
      },
      {
        path: 'opac',
        component: OpacComponent
      },
      {
        path: 'books/reserved',
        component: ReservedBooksComponent
      },
      {
        path: 'books/reserved/transactions',
        component: ReservedBooksTransactionsComponent
      },
      {
        path: 'books/borrowed',
        component: BorrowedBooksComponent
      },
      {
        path: 'books/borrowed/transactions',
        component: BorrowedBooksTransactionsComponent
      },
      {
        path: 'profile',
        component: ProfileComponent
      },
      {
        path: 'reports/allusers',
        component: ReportsAllusersComponent
      },
      {
        path: 'reports/booksnotreturned',
        component: ReportsBooksnotreturnedComponent
      },
      {
        path: 'reports/userscheckedout',
        component: ReportsCheckedoutComponent
      },
      {
        path: 'reports/bookslost',
        component: ReportsLostComponent
      },
      {
        path: 'reports/usersoverdue',
        component: ReportsOverdueComponent
      },
      {
        path: 'reports/topusers',
        component: ReportsTopusersComponent
      },
      {
        path: 'reports/withfine',
        component: ReportsUserswithfineComponent
      }
    ]
  },
  { path: 'login', component: LoginComponent },
  { path: '', redirectTo: 'login', pathMatch: 'full' },
  { path: '**', redirectTo: 'login', pathMatch: 'full' },
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forRoot(
      appRoutes,
      { enableTracing: false } // <-- debugging purposes only
    ),
  ],
  providers: [{provide: LocationStrategy, useClass: HashLocationStrategy}],
  declarations: [LayoutComponent, HeaderComponent, FooterComponent, SidemenuComponent],
  exports: [LayoutComponent]
})
export class UiModule { }
