import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpClientModule, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ServerrequestService {

  constructor(
  	private httpClient: HttpClient
  	) { }

  APIURL : string = "http://157.230.62.195:8080/api/";


  Login(f){
  	return this.httpClient.post<any>(this.APIURL + 'user/login', (f));
  }

  GetAllMembers(){
    return this.httpClient.get<any>(this.APIURL + 'members');
  }

  GetMember(f){
    return this.httpClient.get<any>(this.APIURL + 'member/' + f);
  }

  UpdateMember(f, g){
    return this.httpClient.put<any>(this.APIURL + 'member/update/' + g , f);
  }

  AddMember(f){
    return this.httpClient.post<any>(this.APIURL + 'user/new', f);
  }

  GetUserByID(f){
    return this.httpClient.get<any>(this.APIURL + 'user/' + f);
  }

  GetAllUserType(){
    return this.httpClient.get<any>(this.APIURL + 'usertypes');
  }

  UpdateUser(f){
    return this.httpClient.put<any>(this.APIURL + 'user/edit', f);
  }

  GetAllDepartments(){
    return this.httpClient.get<any>(this.APIURL + 'departments');
  }

  AddNewDepartment(f){
  	return this.httpClient.post<any>(this.APIURL + 'department/new', f); 
  }

  UpdateDepartment(f){
    return this.httpClient.put<any>(this.APIURL + 'department/edit', (f));
  }

  GetAllAuthors(){
    return this.httpClient.get<any>(this.APIURL + 'authors');
  }

  UpdateAuthor(f,g){
    return this.httpClient.put<any>(this.APIURL + 'author/update/' + g, f);
  }

  // AddAuthor(f){
  //   return this.httpClient.
  // }

  GetAllBooks(){
    return this.httpClient.get<any>(this.APIURL + 'Books');
  }

  BorrowBook(f){
    return this.httpClient.post<any>(this.APIURL + 'book/borrow', f);
  }

  ReserveBook(f){
    return this.httpClient.post<any>(this.APIURL + 'reservation/new', f);
  }

  GetAllReservedBooks(){
    return this.httpClient.get<any>(this.APIURL + 'reservations');
  }

  GetAllActiveReservedBooks(){
    return this.httpClient.get<any>(this.APIURL + 'reservations/active');
  }

  GetAllReservationTransactions(){
    return this.httpClient.get<any>(this.APIURL + 'reservations/transactions');
  }

  CheckoutBook(f,g){
    return this.httpClient.put<any>(this.APIURL + 'reservation/checkout/'+g, f);
  }

  CancelReservation(f){
    return this.httpClient.put<any>(this.APIURL + 'reservation/cancel/'+ f, f);
  }

  GetAllBorrowedBooks(){
    return this.httpClient.get<any>(this.APIURL + 'books/borrowed');
  }

  GetAllActiveBorrowedBooks(){
    return this.httpClient.get<any>(this.APIURL + 'books/borrowed/active');
  }

  GetAllBorrowedTransactions(){
    return this.httpClient.get<any>(this.APIURL + 'books/borrowed/transactions');
  }

  ReturnBook(f){
    return this.httpClient.put<any>(this.APIURL + 'book/return/' + f, f);
  }

  CountActiveMembers(){
    return this.httpClient.get<any>(this.APIURL + 'members/active/total');
  }

  CountBorrowedBooks(){
    return this.httpClient.get<any>(this.APIURL + 'books/borrowed/total');
  }

  CountReservedBooks(){
    return this.httpClient.get<any>(this.APIURL + 'reservations/total');
  }

  CountLostBooks(){
    return this.httpClient.get<any>(this.APIURL + 'books/lost/total');
  }

  CountActiveBorrows(){
    return this.httpClient.get<any>(this.APIURL + 'books/borrowed/total');
  }

  CountActiveReserved(){
    return this.httpClient.get<any>(this.APIURL + 'reservations/reserved/total');
  }

  GetLostBooks(){
    return this.httpClient.get<any>(this.APIURL + 'books/lost');
  }

  GetTopBorrowers(){
    return this.httpClient.get<any>(this.APIURL + 'books/top-borrowers');
  }







}

