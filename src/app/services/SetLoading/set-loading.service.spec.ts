import { TestBed, inject } from '@angular/core/testing';

import { SetLoadingService } from './set-loading.service';

describe('SetLoadingService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [SetLoadingService]
    });
  });

  it('should be created', inject([SetLoadingService], (service: SetLoadingService) => {
    expect(service).toBeTruthy();
  }));
});
