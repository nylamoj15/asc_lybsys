import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HashLocationStrategy, LocationStrategy, CommonModule  } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms'
import {NG_VALUE_ACCESSOR} from "@angular/forms";

import { AppComponent } from './app.component';
import { HomeComponent } from './home/home.component';
import { WorklistComponent } from './pages/worklist/worklist.component';
import { UiModule } from './ui/ui.module';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { Ng2SearchPipeModule } from 'ng2-search-filter';
import { SelectDropDownModule } from 'ngx-select-dropdown'

import { Ng2SmartTableModule } from 'ng2-smart-table';
import { Ng2CompleterModule } from "ng2-completer";
import { FileRenderComponent } from './file-render/file-render.component';
import { NgxSelectModule } from 'ngx-select-ex';
import { StatusComponent } from './pages/status/status.component';
import { MembersComponent } from './pages/members/members.component';
import { BooksComponent } from './pages/books/books.component';
import { LoginComponent } from './login/login.component';
import { SettingsComponent } from './pages/settings/settings.component';
import { SettingsDepartmentsComponent } from './pages/settings-departments/settings-departments.component';
import { SettingsAuthorsComponent } from './pages/settings-authors/settings-authors.component';
import { DashboardComponent } from './pages/dashboard/dashboard.component';
import { ReportsComponent } from './pages/reports/reports.component';
import { OpacComponent } from './pages/opac/opac.component';
import { ReservedBooksComponent } from './pages/reserved-books/reserved-books.component';
import { BorrowedBooksComponent } from './pages/borrowed-books/borrowed-books.component';
import { ReservedBooksTransactionsComponent } from './pages/reserved-books-transactions/reserved-books-transactions.component';
import { BorrowedBooksTransactionsComponent } from './pages/borrowed-books-transactions/borrowed-books-transactions.component';
import { ProfileComponent } from './pages/profile/profile.component';
import { ExcelService } from './services/excel.service';
import { ReportsLostComponent } from './pages/reports-lost/reports-lost.component';
import { ReportsCheckedoutComponent } from './pages/reports-checkedout/reports-checkedout.component';
import { ReportsOverdueComponent } from './pages/reports-overdue/reports-overdue.component';
import { ReportsBooksnotreturnedComponent } from './pages/reports-booksnotreturned/reports-booksnotreturned.component';
import { ReportsTopusersComponent } from './pages/reports-topusers/reports-topusers.component';
import { ReportsUserswithfineComponent } from './pages/reports-userswithfine/reports-userswithfine.component';
import { ReportsAllusersComponent } from './pages/reports-allusers/reports-allusers.component';



const appRoutes: Routes = [
  {
    path: 'home',
    component: HomeComponent,
    children: [
      {
        path: 'worklist',
        component: WorklistComponent
      },
      {
        path: 'status',
        component: StatusComponent
      },
      {
        path: 'members',
        component: MembersComponent
      },
      {
        path: 'books',
        component: BooksComponent
      }
    ]
  },
  { path: 'login', component: LoginComponent },
  { path: '', redirectTo: '/home/members', pathMatch: 'full' },
  { path: '**', redirectTo: '/home/members', pathMatch: 'full' },
];


@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    WorklistComponent,
    FileRenderComponent,
    StatusComponent,
    MembersComponent,
    BooksComponent,
    LoginComponent,
    SettingsComponent,
    SettingsDepartmentsComponent,
    SettingsAuthorsComponent,
    DashboardComponent,
    ReportsComponent,
    OpacComponent,
    ReservedBooksComponent,
    BorrowedBooksComponent,
    ReservedBooksTransactionsComponent,
    BorrowedBooksTransactionsComponent,
    ProfileComponent,
    ReportsLostComponent,
    ReportsCheckedoutComponent,
    ReportsOverdueComponent,
    ReportsBooksnotreturnedComponent,
    ReportsTopusersComponent,
    ReportsUserswithfineComponent,
    ReportsAllusersComponent
  ],
  imports: [
    RouterModule.forRoot(
      appRoutes,
      { enableTracing: false } // <-- debugging purposes only
    ),
    BrowserModule,
    UiModule,
    NgbModule,
    // SelectDropDownModule,
    Ng2SmartTableModule,
    Ng2CompleterModule,
    FormsModule,
    ReactiveFormsModule,
    NgxSelectModule,
    Ng2SearchPipeModule,
    SelectDropDownModule
  ],
  entryComponents: [ FileRenderComponent ],
  providers: [{provide: LocationStrategy, useClass: HashLocationStrategy}, ExcelService],
  bootstrap: [AppComponent]
})
export class AppModule { }
