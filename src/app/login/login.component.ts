import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { ServerrequestService } from '../services/serverrequest.service';
import { RouterModule, Routes, ActivatedRoute, ParamMap, Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

	UserID: string;
	Password: string;
	Message: string;
  MID: string;
  FN: string;

  constructor(
  	public SrvrRqst : ServerrequestService,
  	private router: Router
  	) { }

  ngOnInit() {
  }

  //Login User
  Login(form: NgForm){
  	this.UserID = form.value.UserID;
  	this.Password = form.value.Password;
  	this.SrvrRqst.Login({"id_number": this.UserID, "password": this.Password})
  	.subscribe((res => {
  		console.log(res);
  		if (res.response){
        this.MID = res.member_id;
        this.GetMember();
        localStorage.setItem("UT", res.usertype_id);
        localStorage.setItem("MID", this.MID);

        this.router.navigate(['/home/dashboard']);
  		}
  		else if (res === "Invalid") {
  			this.Message = "UserID or Password Incorrect. Please try again."
  		}
  	}))

  }

  async GetMember(){
    this.SrvrRqst.GetMember(this.MID)
    .subscribe((res => {
      this.FN = res.first_name + " " + res.last_name;
      localStorage.setItem("FN", this.FN);
      console.log(res);
    }))
  }

}
